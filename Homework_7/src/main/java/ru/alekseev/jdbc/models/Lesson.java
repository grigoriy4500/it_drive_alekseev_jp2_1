package ru.alekseev.jdbc.models;

public class Lesson {

    private Integer id;
    private String courses;
    private Integer Course_id;

    public Lesson(Integer id, String courses, Integer course_id) {
        this.id = id;
        this.courses = courses;
        Course_id = course_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourses() {
        return courses;
    }

    public void setCourses(String courses) {
        this.courses = courses;
    }

    public Integer getCourse_id() {
        return Course_id;
    }

    public void setCourse_id(Integer course_id) {
        Course_id = course_id;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "id=" + id +
                ", courses='" + courses + '\'' +
                ", Course_id=" + Course_id +
                '}';
    }
}
