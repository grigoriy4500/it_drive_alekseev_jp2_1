package ru.alekseev.jdbc.models;

public class Course {

    private Integer id;
    private String urok;
    private Integer Lesson_id;

    public Course(Integer id, String urok, Integer lesson_id) {
        this.id = id;
        this.urok = urok;
        Lesson_id = lesson_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrok() {
        return urok;
    }

    public void setUrok(String urok) {
        this.urok = urok;
    }

    public Integer getLesson_id() {
        return Lesson_id;
    }

    public void setLesson_id(Integer lesson_id) {
        Lesson_id = lesson_id;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", urok='" + urok + '\'' +
                ", Lesson_id=" + Lesson_id +
                '}';
    }
}
