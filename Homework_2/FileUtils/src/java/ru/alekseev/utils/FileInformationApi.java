package ru.alekseev.infofile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import ru.alekseev.infofile.FileInformation;


public class FileInformationApi {

	public List<FileInformation> getFilesInformation(File directory) {

		List<FileInformation> fileList = new ArrayList<>();

		File[] files = directory.listFiles();

		for(File item : files) {

			if(item.isDirectory()) { //для папок
				fileList.add(new FileInformation(item.getName()));
			}
			else { //для файлов
				fileList.add(new FileInformation(item.getName(), item.length()));
			}
		}
		return fileList;
	}