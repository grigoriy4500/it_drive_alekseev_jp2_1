package ru.alekseev;

import java.io.File;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FileInformationApi extends FileInformation{

    public FileInformationApi() {

    }


    private HashMap<String,List<FileInformation>> fileInformation = new HashMap<String, List<FileInformation>>();

    public Map <String, List<FileInformation>> getFileInformation(String directoryPath){

        File directory = new File(directoryPath); //открываем папку через переданный путь

        ExecutorService service = Executors.newFixedThreadPool(directory.listFiles().length);
        //создание потоков равное колличеству обьектов в папке

        for (File it : directory.listFiles()){ //проходим по каждому элементу

            final List<FileInformation> filesArray = new ArrayList<FileInformation>();

            if (it.isDirectory()){
                service.execute(new Runnable() {
                    @Override
                    public void run() {
                        File file = new File(it.getPath());
                        for(File item : file.listFiles()){
                            filesArray.add(new FileInformation(item.getName(),item.length()));
                        }
                        fileInformation.put(it.getName(),filesArray);
                        System.out.println("Поток: " + Thread.currentThread().getName() + ". Папка: " + it.getName());
                    }
                });
            }
        }
        service.shutdown();
        return fileInformation;
    }
}
