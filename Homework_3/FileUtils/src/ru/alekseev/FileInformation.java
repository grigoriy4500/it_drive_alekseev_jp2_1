package ru.alekseev;

public class FileInformation {
    private String fileName;
    private long fileSize;

    public FileInformation(String fileName, long fileSize) {
        this.fileName = fileName;
        this.fileSize = fileSize;
    }

    public FileInformation() {
    }

    public String getFileName() {
        return fileName;
    }

    public long getFileSize() {
        return fileSize;
    }

}
