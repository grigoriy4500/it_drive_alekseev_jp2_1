package ru.alekseev;


import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

	    FileInformationApi fileInformationApi = new FileInformationApi();

	    fileInformationApi.getFileInformation("/Users/grigoriy/Desktop/it_drive_alekseev_jp2_1/Homework_1/FileUtils/src/java/ru/alekseev/infofile/");

        for (Map.Entry<String, List<FileInformation>> s : fileInformationApi
                .getFileInformation("/Users/grigoriy/Desktop/it_drive_alekseev_jp2_1/Homework_1/FileUtils/src/java/ru/alekseev/infofile/")
                .entrySet()) {
            for(FileInformation item : s.getValue()){
                System.out.println(s.getKey() + " " + item.getFileName() + " " + item.getFileSize());
            }
        }
    }
}
