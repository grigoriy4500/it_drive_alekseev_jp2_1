package main.java;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Argument {
    @Parameter(names = {"--url"})
    public String url;
}