package main.java;

import com.beust.jcommander.JCommander;
import ru.alekseev.FileInformation;
import ru.alekseev.FileInformationApi;

import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        FileInformationApi fileInformationApi = new FileInformationApi();

        Argument argument = new Argument();
        JCommander.newBuilder()
                .addObject(argument)
                .build()
                .parse(args);

        for (Map.Entry<String, List<FileInformation>> s : fileInformationApi
                .getFileInformationI(argument.url)
                .entrySet()) {
            for(FileInformation item : s.getValue()){
                System.out.println(s.getKey() + " " + item.getFileName() + " " + item.getFileSize());
            }
        }
    }
}
