create table customer
(
  id serial primary key,
  first_name char(20),
  last_name char(20),
  mail char(30),
  pass char(20)
);

create table chat
(
    id serial primary key,
    chat_name char(20),
    create_date timestamp
);

create table message
(
    id serial primary key,
    text char(50),
    customer_id integer,
    foreign key (customer_id) references customer(id),
    chat_id integer,
    foreign key (chat_id) references chat(id),
    time timestamp
);
