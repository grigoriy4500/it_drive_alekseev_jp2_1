package ru.alekseev.infofile;

import java.util.ArrayList;
import java.util.List;


public class FileInformation {
	private String fileName;
	private int fileSize;

	public FileInformation() {

	}

	public FileInformation(String fileName) {
		this.fileName = fileName;
	}

	public FileInformation(String fileName, int fileSize) { // конструктор для файла
		this.fileName = fileName;
		this.fileSize = fileSize;
	}

	public String getFileName() {
		return fileName;
	}

	public int getFileSize() {
		return fileSize;
	}

	public void setName(String newFileName) {
		this.fileName = newFileName;
	}

	public void setSize(int newFileSize) {
		this.fileSize = newFileSize;
	}
}
