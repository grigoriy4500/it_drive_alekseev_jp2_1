package ru.alekseev.infofile.app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Argument {

    @Parameter(names = {"--dirUrl"})
    String dirUrl;

    public String getDirUrl() {
        return dirUrl;
    }
}
