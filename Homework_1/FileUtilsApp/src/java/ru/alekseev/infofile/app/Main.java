package ru.alekseev.infofile.app;

import com.beust.jcommander.JCommander;
import ru.alekseev.infofile.FileInformation;
import ru.alekseev.infofile.FileInformationApi;

import java.io.File;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        FileInformationApi api = new FileInformationApi();
        Arguments arg = new Arguments();

        JCommander.newBuilder()
                .addObject(arg)
                .build()
                .parse(args);

        List<FileInformation> filesInformation = api.getFilesInformation(new File(arg.getDirUrl()));

        for (FileInformation i : filesInformation) {
            
            System.out.println(i.getFileName() + " " + i.getFileSize());

        }
    }
}
